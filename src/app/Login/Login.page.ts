import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { user } from '../models/Login.model';
import { UserService } from '../services/user.service';
import {
  HttpClientModule,
  HttpClient,
  HttpParams,
  HttpHeaders,
} from '@angular/common/http';

//Initializing the component
@Component({
  selector: 'app-login',
  templateUrl: './Login.page.html',
  styleUrls: ['./Login.page.css'],
})
export class LoginPage implements OnInit {
  //initializing variables to be used in this component
  name: String;
  fetchedUsers: any;
  checkedUser: any;
  currentUser: any;
  //initializing constructor
  constructor(
    private readonly UsersService: UserService,
    private router: Router,
    private http: HttpClient
  ) {}
  ngOnInit(): void {
    //fetching users from API on init
    this.getUser();
  }
  //function to post a new user to the trainer API
  apikey = 'Dr+k1aDWF0yYCssJalBCCw==';
  postUser() {
    let headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'X-API-Key': this.apikey,
    });
    let options = { headers: headers };
    let url = 'https://jt-trivia-api.herokuapp.com/trainers';
    this.http
      .post(
        url,
        {
          name: this.name,
          pokemon: [],
        },
        options
      )
      .subscribe((result) => {
        localStorage.setItem('User', JSON.stringify(result));
      });
  }
  //getting the users from the API
  getUser() {
    this.http
      .get<user[]>('https://jt-trivia-api.herokuapp.com/trainers')
      .subscribe((users) => {
        this.fetchedUsers = users;
      });
  }
  //getting one specific user(used when logging in to an existing user)
  getOneUser(username) {
    this.http
      .get<user[]>(
        `https://jt-trivia-api.herokuapp.com/trainers?name=${username}`
      )
      .subscribe((users) => {
        this.currentUser = users[0];

        localStorage.setItem('User', JSON.stringify(this.currentUser));
      });
  }

  //function to check if user already exists or not. Create user if it doesnt or log it in if it exists
  checkuser(username) {
    this.checkedUser = this.fetchedUsers.find((x) => x.name === username);
    if (this.checkedUser) {
      this.getOneUser(username);
    } else {
      this.postUser();
    }
  }

  //button function for loggin in user
  public onSubmit(createForm: NgForm): void {
    localStorage.setItem('Token', 'LoggedIn');
    this.name = createForm.value.name;
    const jsondata = JSON.stringify(createForm.value);
    this.checkuser(this.name);
    this.router.navigate(['/catalogue']);
  }
}
