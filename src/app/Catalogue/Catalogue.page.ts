import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Observable, tap } from 'rxjs';
import { user, pokemon, poke } from '../models/Login.model';
import { DataService } from '../services/data.service';
import { UserService } from '../services/user.service';
import { Router } from '@angular/router';

//Initializing component
@Component({
  selector: 'app-Catalogue',
  templateUrl: './Catalogue.page.html',
  styleUrls: ['./Catalogue.page.css'],
})
export class CataloguePage implements OnInit {
  //initializing variables for use in component
  pokemons: pokemon[] = [];
  poke: poke[] = [];
  currentuser: user[] = [];
  id: string;
  //initializing contructor
  constructor(
    private readonly UsersService: UserService,
    private dataService: DataService,
    private http: HttpClient,
    private router: Router
  ) {}
  // getting information from service on init
  ngOnInit(): void {
    this.dataService.fetchPoke().subscribe((response: any) => {
      response.results.forEach((result) => {
        this.dataService.getMoreInfo(result.name).subscribe((resp: any) => {
          this.pokemons.push({
            name: resp.name,
            height: resp.height,
            sprites: resp.sprites,
            weight: resp.weight,
          });
        });
      });
    });
  }

  //Button function for adding pokemon to trainer API and sending out alert that you caught a pokemon
  public clickPokemon(xd): void {
    this.id = JSON.parse(localStorage.getItem('User')).id;
    this.poke = [{ name: xd.name, url: xd.sprites.front_default }];
    this.getUser().subscribe(() => {
      this.catchPokemon(this.currentuser, this.poke);
    });
    console.log('Added pokemon!');
    alert(`You have caught: ${xd.name}`);
  }
  //Button function to log out
  public logout() {
    localStorage.clear();
    this.router.navigate(['/']);
  }

  //Function to update trainer API with the caught pokemon
  apikey = 'Dr+k1aDWF0yYCssJalBCCw==';
  public catchPokemon(pokk, poke): void {
    let headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'X-API-Key': this.apikey,
    });
    let options = { headers: headers };
    let url = ` https://jt-trivia-api.herokuapp.com/trainers/${this.id}`;
    this.http
      .patch(
        url,
        {
          pokemon: [...pokk, ...poke],
        },
        options
      )
      .subscribe((result) => {});
  }
  //getting the user from trainer api so that we can update the pokemons caught
  getUser(): Observable<any> {
    return this.http
      .get<any>(` https://jt-trivia-api.herokuapp.com/trainers/${this.id}`)
      .pipe(
        tap((response) => {
          this.currentuser = response.pokemon;
        })
      );
  }
}
