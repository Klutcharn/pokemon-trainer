export interface user {
  id: number;
  username: string;
  pokemon: string[];
}

export interface pokemon {
  name: string;
  height: number;
  weight: number;
  sprites: any;
}

export interface poke {
  name: string;
  url: string;
}
