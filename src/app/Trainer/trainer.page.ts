import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Observable, tap } from 'rxjs';
import { poke } from '../models/Login.model';

//Initializing component
@Component({
  selector: 'app-Trainer',
  templateUrl: './trainer.page.html',
  styleUrls: ['./trainer.page.css'],
})
export class TrainerPage implements OnInit {
  //initializing variables to use in component
  caughtPokemons: poke[] = [];
  id: string;
  username: string;
  //initialising constructor
  constructor(private http: HttpClient, private router: Router) {}
  ngOnInit(): void {
    //getting username, id from local storage
    this.id = JSON.parse(localStorage.getItem('User')).id;
    this.username = JSON.parse(localStorage.getItem('User')).name;
    //getting the whole user from trainer API to get caught pokemons
    this.getUser();
  }
  //API fetch to get all caught pokemons
  getUser(): void {
    this.http
      .get<any>(`https://jt-trivia-api.herokuapp.com/trainers/${this.id}`)
      .subscribe((users) => {
        this.caughtPokemons = users.pokemon;
        console.log(this.caughtPokemons);
      });
  }
  //button function to release the pokemon clicked
  releasePoke(pokemon): void {
    this.caughtPokemons.forEach((item, index) => {
      if (item === pokemon) this.caughtPokemons.splice(index, 1);
    });
    this.updateCatchedPokemon();
  }
  //API post function to update the trainer API
  apikey = 'Dr+k1aDWF0yYCssJalBCCw==';
  public updateCatchedPokemon(): void {
    let headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'X-API-Key': this.apikey,
    });
    let options = { headers: headers };
    let url = ` https://jt-trivia-api.herokuapp.com/trainers/${this.id}`;
    this.http
      .patch(
        url,
        {
          pokemon: this.caughtPokemons,
        },
        options
      )
      .subscribe((result) => {});
  }
  //button function to log out
  public logout() {
    localStorage.clear();
    this.router.navigate(['/']);
  }
}
