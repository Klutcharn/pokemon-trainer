import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { user } from '../models/Login.model';

@Injectable({
  providedIn: 'root',
})
export class UserService {
  private _users: user[] = [];

  private _error: string = '';

  constructor(private readonly http: HttpClient) {}
  public fetchUser(): void {
    this.http
      .get<user[]>('https://jt-trivia-api.herokuapp.com/trainers')
      .subscribe(
        (users) => {
          this._users = users;
        },
        (error: HttpErrorResponse) => {
          this._error = error.message;
        }
      );
  }
  public getUsers(): user[] {
    return this._users;
  }

  public error(): string {
    return this._error;
  }
}
