# Pokemon

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 13.2.1.


<h2>Information</h2>

<dl>
         <dt><b>Login page</b></dt>
         <dd>An user is logged into or added to the API on depending on if user exists or not</dd>
         <dd>If you have logged in already you will be directed to the catalogue page via local storage</dd>
         <dt><b>Catalogue page</b></dt>
         <dd>Here you will see all pokemons from first generation(151 pokemons)</dd>
         <dd>Click a "pokemon card" to collect a pokemon</dd>
         <dd>You will get a notification that you have collected chosen pokemon</dd>
         <dd>You cannot access this page without being logged in</dd>
         <dt><b>Trainer page</b></dt>
         <dd>All caught pokemons will be displayed here</dd>
        <dd>You have the oppertunity to release the pokemon here which also deletes the pokemon from trainer-api(we know that in bigger project you should make it as released and then have a deletion set every day ect, but for this small project we decided to remove it fully each click)</dd>
        <dd>You cannot access this page without being logged in</dd>
</dl>


## Usage

Clone down this repository. You will need `node` and `npm` installed globally on your machine.  

```
Installation:

`npm install -g @angular/cli`  

To Start Server:

`ng serve`  

To Visit App:

`http://localhost:4200/`  

```


## Maintainers

 * Joachim Nilsson - https://gitlab.com/Klutcharn
 * Thomas Annerfeldt - https://gitlab.com/thumas

